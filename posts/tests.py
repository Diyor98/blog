from django.test import TestCase
from django.contrib.auth.models import User

from posts.models import Post


class PostTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        testuser1 = User.objects.create_user(
            username='testuser1',password='password'
        )

        testuser1.save()

        post = Post.objects.create(
            author = testuser1, title='My post',body ='body'
        )

        post.save()

    def test_blog_content(self):
        post = Post.objects.get(id=1)
        author = f"{post.author}"
        title = f"{post.title}"
        body = f"{post.body}"

        self.assertEquals(title,'My post')
        self.assertEquals(author,"testuser1")
        self.assertEquals(body,'body')

