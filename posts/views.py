from django.contrib.auth import get_user_model
from rest_framework.viewsets import ModelViewSet



from posts.models import Post
from posts.serializers import PostSerializer, UserSerializer
from posts.permissions import IsAuthorOrReadOnly

class PostViewSet(ModelViewSet):
    permission_classes = [IsAuthorOrReadOnly,]
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class UserViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer











# With ordinary views

# from rest_framework.generics import (
#     ListCreateAPIView,
#     RetrieveUpdateDestroyAPIView
# )
# from rest_framework.permissions import IsAuthenticated
# class PostList(ListCreateAPIView):

#     queryset = Post.objects.all()
#     serializer_class = PostSerializer

# class PostDetail(RetrieveUpdateDestroyAPIView):
#     permission_classes = [IsAuthorOrReadOnly,IsAuthenticated]
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer

# class UserList(ListCreateAPIView):
#     queryset = get_user_model().objects.all()
#     serializer_class = UserSerializer

# class UserDetail(RetrieveUpdateDestroyAPIView):
#     queryset = get_user_model().objects.all()
#     serializer_class = UserSerializer