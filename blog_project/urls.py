
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/',include('posts.urls')),
    path('api-auth/', include('rest_framework.urls')), # Login/logout in API (for testing?)
    path('api/v1/rest-auth/',include('rest_auth.urls')), #login/logout/password reset endpoints!
    path('api/v1/rest-auth/registration/',
          include('rest_auth.registration.urls')),
]
